import java.util.Scanner;
import java.util.ArrayList;
public class whileLoop{
	public static void cvHex2Ascii(ArrayList<String> hexstring){
		//รับค่าString and Convert to Integer
		System.out.println("print For Loop ArrayList");
		ArrayList<Integer> decimal = new ArrayList<Integer>();	//สร้างobjเพื่อเก็บค่าintที่แปลงจากString
		for(int i = 0; i < hexstring.size(); i++){
			decimal.add(Integer.parseInt(hexstring.get(i), 16));	//แปลงStringเป็นintโดยบอกให้รู้ว่าStringนี้หมายถึงเลขฐาน16 ให้แปลงเป็นฐาน10 และเพิ่มลงในobj ArrayListสมาชิกทั้งหมดเป็นInteger
			System.out.printf("ASCII code: %d -> Symbol: %c\n",decimal.get(i) ,decimal.get(i));
		}
		System.out.println("print While Loop ArrayList");
		int count = 0;
		while(hexstring.size() > count){
			decimal.add(Integer.parseInt(hexstring.get(count), 16));
			System.out.printf("ASCII code: %d -> Symbol: %c\n",decimal.get(count), decimal.get(count));
			count++;
		}
		//ยังมีLoop AraayListอีก 2วิธี --Advance For และIterator
	}

	/*public static void cv2(int dec, int dec2){
	 	//รับค่าintมาตรงๆ
		System.out.println("Receive Integer");
		ArrayList<Integer> decimo = new ArrayList<Integer>();
		while(dec < dec2){
			decimo.add(dec);
			System.out.println(decimo.get(dec));
			dec++;
		}
		int c = 0;
		while(decimo.size() > c){
			System.out.println(decimo.get(c));
			c++;
		} //USELESS Method!
	}*/

	public static void convertD2H(int dec, int dec2){
		//แปลงเลขฐาน10 เป็นฐาน16
		//ใช้ArrayListเนื่องจากไม่ทราบขนาดของArrayล่วงหน้า และกำหนด data typeให้ข้อมูลในArrayเป็นประเภทเดียวกันด้วย<>
		//Arrayธรรมดาต้องกำหนดขนาดล่วงหน้าเสมอ และไม่สามารถมีdata typeที่แตกต่างกันได้
		ArrayList<String> hexs = new ArrayList<String>();
		while(dec <= dec2){
			//System.out.printf("Decimal num: %d -> Hex num: %X\n",dec ,dec);
			String hex = Integer.toHexString(dec);	//Convert integer to string
			hex = hex.toUpperCase();
			System.out.printf("Decimal num: %d -> Hex num: %s\n",dec ,hex);
			hexs.add(hex);	//เพิ่มข้อมูลลงในArrayList
			dec++;
		}
		//cv2(dec, dec2);
		System.out.print("Press (c)To convert Hex to Decimal number and print ASCII code or Press another keys to exit? -> ");
		Scanner in = new Scanner(System.in);
		char enter = in.next().charAt(0);
		if(enter == 'c'){
			cvHex2Ascii(hexs);
		}
		else;

		in.close();
	}

	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.print("Input num start: ");
		int dec = obj.nextInt();
		System.out.print("Input num end: ");
		int dec2 = obj.nextInt();

		convertD2H(dec, dec2);
		obj.close();
	}
}
