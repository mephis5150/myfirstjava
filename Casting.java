class Casting{
	public static void main(String[] args){
	/*การแปลงชนิดข้อมูล
	  Widening Casting แปลงข้อมูลขนาดเล็ก -> ใหญ่
	  Narrowing Casting แปลงข้อมูลขนาดใหญ่ -> เล็ก*/
	
	int num = 9;
	double d = num;
	System.out.println(d);	//Widening แปลง Auto

	double num2 = 9.55;
	int i = (int)num2;	//Narrowing ต้องระบุ num2 เป็น int
	System.out.println(i);

	System.out.println("example...");
	System.out.println("String -> int");
	String price  = "10";
	int    price2 = Integer.parseInt(price);
	System.out.println(price2+6);

	System.out.println("String -> float");
	float f = Float.parseFloat("30.36");
	System.out.println(f+3);
	}
}
