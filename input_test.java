import java.util.Scanner;

class input_test{
	public static void main(String[] args){
		Scanner myObj = new Scanner(System.in);
		//Scannerเป็นclass ต้องทำให้เป็นobjเพื่อเรียกใช้

		System.out.print("input your name : ");
		String name = myObj.nextLine();	//nextLine()อ่านข้อมูลที่inputเป็นString
		if(name.equals("game")||name.equals("Game")||name.equals("เกมส์")||name.equals("เกม")){
			System.out.println("สวัสดี ไอ้ก้างชื่อ "+name);
		}
		else{
			System.out.println("Hi,"+name);
		}
		System.out.print("input your age : ");
		int age = myObj.nextInt(); //อ่านค่าข้อมูลinput เป็นint
		System.out.println("your age is "+age+" years old");

		myObj.close();
	}
}
