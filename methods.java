import java.util.Scanner;
public class methods{
	public static double rectangle(double width, double height){
		//รับค่าdouble และreturnกลับเป็นdouble
		return width*height;
	}
	public static int square(double width){
		//รับค่าdouble และreturnกลับเป็นint
		double area2 = width*width;
		int a = (int)area2;	//บังคับให้area2 เป็นint
		return a;
	}
	public static float triangle(float j, float k){
		//ทดสอบค่าfloat
		return .5f*j*k;
	}
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Input width: ");
		double width = input.nextDouble();
		System.out.println("width = "+width);
		System.out.print("Input height: ");
		double height = input.nextDouble();
		System.out.println("height = "+height);

		double area = rectangle(width, height);
		System.out.println("rectangle area: ("+width + "*" +height+") = "+area);
		int area2 = square(width);
		System.out.println("square area: ("+width+ "*" +width+") = "+area2);
		float tri_area = triangle(4, 5.5f);
		System.out.println("triangle area: (1/2 * base(j) * height(k)) = "+tri_area);

		input.close();
	}
}
