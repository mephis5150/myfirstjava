import java.util.Scanner;

public class Salary{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Input your hour : ");
		int hour = sc.nextInt();
		int salary;
		if(hour <=10){
			salary = hour*100;
		}
		else if(hour <= 20){
			salary = hour*200;
		}
		else {
			salary = hour*300;
		}
		System.out.println("Salary = "+salary);
		sc.close();
	}
}
