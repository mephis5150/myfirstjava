public class ExString{
    public static void main(String[] args) {
        //1.
        System.out.println("ข้อที่ 1");
        String name = "Patipon Pankaew";
        System.out.println("My name is "+name);

        //2.
        System.out.println("\nข้อที่ 2");
        System.out.println("all length = "+name.length());
        System.out.println("length without blank space = "+name.replace(" ","").length());

        //3.
        System.out.println("\nข้อที่ 3");
        for(int i=0; i <name.length(); i++) {
            System.out.println(""+name.charAt(i));
        }

        //4.
        System.out.println("\nข้อที่ 4");
        System.out.println("all length = "+name.length());

        //5.
        System.out.println("\nข้อที่ 5");
        String msg = "http://rmutsv.ac.th/program/comsci_it/";
        System.out.println("message = "+msg);
        //for(String spl_msg : msg.split("/")) System.out.println(spl_msg);
        System.out.println("/ in message = "+msg.split("/").length);
        System.out.println("replace / whit * in message = "+msg.replace("/","*"));

        //6.
        System.out.println("\nข้อที่ 6");
        StringBuffer txt = new StringBuffer("Chris Angle");
        System.out.println("Before insert text : "+ txt);
        System.out.println("After insert text : "+txt.insert(5,"topher"));

        //7.
        System.out.println("\nข้อที่ 7");
        String str = "Rajamangala University of Technology Srivijaya";
        System.out.println("str = "+str);
        System.out.println("R&r in str = "+str.split("r").length);

        //8.
        System.out.println("\nข้อที่ 8");
        System.out.println("str to UpperCase : "+str.toUpperCase());

        //9.
        System.out.println("\nข้อที่ 9");
        System.out.println("str to LowerCase : "+str.toLowerCase());
    }

}