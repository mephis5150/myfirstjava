import java.util.Scanner;
public class ForLoop{
	public static int sum(int numst, int numend){
		/*บวกตัวเลขที่รับค่ามา จาก...ถึง... และreturnผลลัพธ์
		  ex. 1 to 3 -> 1+2+3 = 6 */
		int result = 0;
		for(int i = numst; i <= numend; i++){
			result = result+i;
		}
		return result;	//ค่าint เก็บค่าตั้งแต่ (-2^31) to ((2^31)-1)
	}

	public static void asciiTable(int numst, int numend){
		/*ตารางASCII  แสดงตัวอักษรตามค่าตัวเลขที่รับมา จาก...ถึง...*/
		for(int charac = numst; charac <= numend; charac++){
			System.out.printf("%d -> %c\n",charac ,(char)charac);
		}
	}
	public static void main(String[] args){
		Scanner obj = new Scanner(System.in);
		System.out.print("Input num start: ");
		int numst = obj.nextInt();
		System.out.print("Input num end: ");
		int numend = obj.nextInt();

		System.out.println(sum(numst, numend));
		asciiTable(numst, numend);

		obj.close();
	}
}
